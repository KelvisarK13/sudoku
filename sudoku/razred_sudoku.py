__author__ = 'KelvisarK13'

from random import *
from time import clock

class Sudoku():
    def __init__(self, sudoku_seznam, zacetni_sudoku = None):
        """Prime seznam seznamov, ki predstavlja sudoku. Atribut število_števk pove, koliko mest je zasedenih."""
        self.sudoku = sudoku_seznam
        self.deveterica = [x for x in range(1, 10)]
        if not zacetni_sudoku:
            self.zacetni_sudoku = [[x for x in vrstica] for vrstica in sudoku_seznam]
        else:
            self.zacetni_sudoku = zacetni_sudoku

        self.resitve = None


    def dodaj_stevilko(self, stevilka, i, j):
        """doda številko na (i,j)-to mesto v sudoku"""
        self.sudoku[i][j] = stevilka

    def odstrani_stevilko(self, i, j):
        """funkcija, ki na (i,j)-tem mestu odstrani številko"""
        self.dodaj_stevilko(None, i, j)

    def komplement_vrstica(self, i):
        """števila, ki se ne pojavijo v i-ti vrstici"""
        komplement = []
        presek = []

        for el in self.sudoku[i]:
            if el in self.deveterica:
                presek.append(el)
        for stevilka in self.deveterica:
            if stevilka not in presek:
                komplement.append(stevilka)
        return komplement

    def komplement_stolpec(self, j):
        """števila, ki se ne pojavijo v j-tem stolpcu"""
        komplement = []
        presek = []
        
        for i in range(0,9):
            if self.sudoku[i][j] in self.deveterica:
                presek.append(self.sudoku[i][j])
        for stevilka in self.deveterica:
            if stevilka not in presek:
                komplement.append(stevilka)
        return komplement

    def komplement_kvadratek(self, i, j):
        """števila, ki se ne pojavijo v kvadratku 3x3, ki ga določa element v i-ti vrstici in j-tem stolpcu"""
        komplement = []
        presek = []

        ri = (i//3)*3
        rj = (j//3)*3

        for v in range(ri, ri+3):
            for s in range(rj, rj+3):
                if self.sudoku[v][s] in self.deveterica:
                    presek.append(self.sudoku[v][s])
        for stevilka in self.deveterica:
            if stevilka not in presek:
                komplement.append(stevilka)
        return komplement

    def moznosti(self, i, j):
        """števila, ki jih lahko zapišemo v prostor na (i,j)-tem mestu"""
        if self.sudoku[i][j] != None:
            return []
        a = set(self.komplement_kvadratek(i, j))
        b = set(self.komplement_vrstica(i))
        c = set(self.komplement_stolpec(j))
        return list((a&b)&c)


    def sudoku_resen(self):
        """funkcija, ki preveri, ali je sudoku rešen (v celoti zapolnjen)"""
        for vrstica in self.sudoku:
            for element in vrstica:
                if element == None:
                    return False
        return True


    def resi_sudoku(self, vseResitve=[]):
        """algoritem za reševanje sudokuja, vrne vse možne rešitve"""
        if self.sudoku_resen():
            return vseResitve + [[[x for x in vrstica] for vrstica in self.sudoku]]
        for i in range(0,9):
            for j in range(0,9):
                if self.sudoku[i][j] != None:
                    continue
                mozne = self.moznosti(i,j)
                if mozne == []:
                    return
                for moznost in mozne:
                    self.dodaj_stevilko(moznost, i, j)
                    c = self.resi_sudoku(vseResitve)
                    if c:
                        vseResitve = c
                self.odstrani_stevilko(i, j)
                if i==0 and j==0:
                    return vseResitve
                return vseResitve


    def vse_resitve(self):
        """funkcija, ki si zapomni vse rešitve, ki jih vrača resi_sudoku, in njihovo dolžino"""
        resitve = self.resi_sudoku()
        self.resitve = resitve

    def prva_resitev(self):
        """algoritem za reševanje sudokuja, vrne prvo rešitev"""
        if self.sudoku_resen():
            return [[x for x in vrstica] for vrstica in self.sudoku]
        for i in range(0,9):
            for j in range(0,9):
                if self.sudoku[i][j] != None:
                    continue
                mozne = self.moznosti(i,j)
                if mozne == []:
                    return
                for moznost in mozne:
                    self.dodaj_stevilko(moznost, i, j)
                    c = self.prva_resitev()
                    if c:
                        return c
                self.odstrani_stevilko(i, j)
                return

    def stevilo_resitev(self, vseResitve=0):
        """vrne ali 0 (nerešljiv sudoku) ali 1 (enolično rešljiv sudoku) ali 2 (sudoku ima več rešitev)"""
        if self.sudoku_resen():
            return vseResitve + 1
        for i in range(0,9):
            for j in range(0,9):
                if self.sudoku[i][j] != None:
                    continue
                mozne = self.moznosti(i,j)
                if mozne == []:
                    return
                for moznost in mozne:
                    self.dodaj_stevilko(moznost, i, j)
                    c = self.stevilo_resitev(vseResitve)
                    if c and c > 1:
                        self.odstrani_stevilko(i, j)
                        return c
                    if c:
                        vseResitve = c
                self.odstrani_stevilko(i, j)
                return vseResitve

def poln_sudoku():
    """funkcija, ki z naključnim vpisom 70 številk vrne prvi možen poln sudoku"""
    while True:
        s = Sudoku([[None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None]])
        for x in range(70):
            #za število 70 program porabi najmanj časa (določeno s poskusi)
            i = randint(0, 8)
            j = randint(0, 8)

            opcije = s.moznosti(i,j)
            if not opcije:
                continue
            dodaj = opcije[0]
            s.dodaj_stevilko(dodaj, i, j)
        nakljucna = s.prva_resitev()
        if nakljucna:
            return nakljucna


def naredi_sudoku(tezavnost):
    """funkcija, ki z odvzemanjem (81-'tezavnost') števk iz polnega sudokuja naredi sudoku, primernega za reševanje"""
    sudoku = Sudoku(poln_sudoku())
    indeksi = [(i, j) for i in range(0, 9) for j in range(0, 9)]
    shuffle(indeksi)
    izbrisani = []
    for (i, j) in indeksi:
        stevilka = sudoku.sudoku[i][j]
        sudoku.odstrani_stevilko(i, j)
        st = sudoku.stevilo_resitev()
        if st != 1:
            sudoku.dodaj_stevilko(stevilka, i, j)
            continue
        izbrisani.append((i,j))
        if len(izbrisani)==(81-tezavnost):
            break
    return [[x for x in vrstica] for vrstica in sudoku.sudoku]







## TESTIRANJE KODE ##

sudoku0 = [[None, None, None, None, None, None, None, None, None],
[None, None, None, None, None, None, None, None, None],
[None, None, None, None, None, None, None, None, None],
[None, None, None, None, None, None, None, None, None],
[None, None, None, None, None, None, None, None, None],
[None, None, None, None, None, None, None, None, None],
[None, None, None, None, None, None, None, None, None],
[None, None, None, None, None, None, None, None, None],
[None, None, None, None, None, None, None, None, None]]
sudoku1 = [[None,None,5,None,None,3,None,4,None],
         [None,None,7,None,None,9,1,2,None],
         [None,2,None,None,None,None,None,None,None],
         [None,5,None,8,1,6,None,None,3],
         [4,None,None,3,None,7,None,None,6],
         [1,None,None,4,9,2,None,5,None],
         [None,None,None,None,None,None,None,3,None],
         [None,3,4,5,None,None,8,None,None],
         [None,8,None,2,None,None,9,None,None]]
sudoku2 = [[None, None, None, None, 3, 7, 6, None, None],
[None, None, None, 6, None, None, None, 9, None],
[None, None, 8, None, None, None, None, None, 4],
[None, 9, None, None, None, None, None, None, 1],
[6, None, None, None, None, None, None, None, 9],
[3, None, None, None, None, None, None, 4, None],
[7, None, None, None, None, None, 8, None, None],
[None, 1, None, None, None, 9, None, None, None],
[None, None, 2, 5, 4, None, None, None, None]]
sudoku3 = [[8, 1, 5, 7, None, 3, 6, None, 9],
           [3, None, 7, 6, 8, 9, 1, None, 5],
           [6, None, 9, 1, None, 5, 3, 8, 7],
           [7, 5, None, 8, 1, 6, None, 9, 3],
           [None, 9, 8, 3, 5, 7, None, 1, 6],
           [1, 6, 3, None, 9, None, 7, 5, 8],
           [None, 7, 1, 9, 6, 8, 5, 3, None],
           [9, 3, None, 5, 7, 1, 8, 6, None],
           [5, 8, 6, None, 3, None, 9, 7, 1]]
testni = [
    [5, 9, None, None, None, 3, None, 4, None],
[None, None, None, None, None, None, None, 9, 1],
[None, None, 8, None, None, None, 6, 3, None],
[None, None, None, None, 6, 8, None, 2, None],
[2, 8, 6, 7, None, None, None, None, 3],
[None, None, 4, None, None, 5, None, None, 8],
[8, None, 9, None, None, 2, None, None, 6],
[4, None, None, 1, None, 9, None, None, None],
[None, None, None, 8, 4, None, 1, None, None]
]


# a = Sudoku(sudoku1)
# vrni = a.resi_sudoku()
# print(vrni)


# k = Sudoku(sudoku3)
# vrni = k.resi_sudoku()
# for x in vrni:
#     print(x)
# print(len(vrni))



# b = Sudoku(sudoku2)
# vrni = b.resi_sudoku()
# print(vrni)

# c = Sudoku(sudoku1).komplement_kvadratek(2,4)
# d = Sudoku(sudoku1).komplement_stolpec(4)
# e = Sudoku(sudoku1).komplement_vrstica(2)
# f = Sudoku(sudoku1).moznosti(2,4)

# print(c, d, e, f)

# k = Sudoku(sudoku1)
# vrni = k.prva_resitev()
# for x in vrni:
#     print(x)


# a = clock()
# for x in poln_sudoku():
#     print(x)
# b = clock()
# print(b-a)

# t = naredi_sudoku(35)
# for x in t.sudoku:
#     print(x)


# s=Sudoku(testni)
# sez = s.resi_sudoku()
# for x in sez[0]:
#     print(x)
# print(len(sez))
#
# print("steviloresitev:", Sudoku(sudoku3).hitre_resitve(), "resiSudoku", len(Sudoku(sudoku3).resi_sudoku()))
# print(Sudoku(sudoku3).stevilo_resitev() == len(Sudoku(sudoku3).resi_sudoku()))
# print(Sudoku(testni).stevilo_resitev() == len(Sudoku(testni).resi_sudoku()))


