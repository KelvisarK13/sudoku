__author__ = 'KelvisarK13'

# -*- encoding: utf-8 -*-

from tkinter import *
from razred_sudoku import *


class GraficniSudoku:
    def __init__(self, master):
        master.title('Sudoku')

        self.sudoku = None
        self.vnesena_stevilka = None
        self.zadnja_vnesena = None
        self.tezavnost = None
        self.se_igra = False

        self.napis = StringVar(value='')
        labelNapis = Label(master, textvariable=self.napis)
        labelNapis.grid(row=1, column=0, columnspan=2, sticky=E+W)

        self.navodila = ["""Sudoku je logična uganka,""",
                        """katere cilj je zapolniti kvadratno mrežo velikosti 9 × 9 s števili od 1 do 9.""",
                        """Vsako število se lahko pojavi točno enkrat v vsakem stolpcu,""",
                        """vsaki vrstici in vsakem manjšem kvadratu velikosti 3 × 3.""",
                        """Za enolično rešitev poskrbijo (na začetku) že zapolnjena mesta.""",
                        """ """,
                        """Uporaba aplikacije:""",
                        """Za začetek igre iz menija 'Nova igra' izberite nivo težavnosti.""",
                        """Za vpis številke na prazno mesto v sudokuju izberite številko s tipkovnice""",
                        """in jo z levim klikom miške na željeno mesto vnesite v polje.""",
                        """Zadnjo potezo lahko razveljavite s tipko 'Backspace'""",
                        """ali z ustrezno izbiro iz menija 'Razveljavi'.""",
                        """Poljubno potezo razveljavite z vnosom nove številke na željeno mesto.""",
                        """Če želite zopet igrati isto igro, z menija 'Razveljavi' izberite možnost 'Razveljavi igro'.""",
                        """Med reševanjem lahko spremljate pravilnost svojih rešitev z menijem 'Pomoč'""",
                        """ ter 'Preveri pravilnost vnosov'.""",
                        """Rešitev sudokuja najdete v istem meniju.""",
                        """ """,
                        """Veliko uspeha pri reševanju!"""]

        #meni#
        menu = Menu(master)
        master.config(menu=menu)


        meni = Menu(menu)
        meni1 = Menu(menu)
        meni3 = Menu(menu)
        meni4 = Menu(menu)
        meni5 = Menu(menu)

        menu.add_cascade(label="Navodila", menu=meni5)
        meni5.add_command(label="Pokaži navodila", command=lambda: self.narisi_navodila())
        meni5.add_command(label="Skrij navodila", command=lambda: self.narisi_sudoku())

        menu.add_cascade(label="Nova igra", menu=meni)
        meni.add_command(label="Easy", command=lambda: self.nova_igra(50))
        meni.add_command(label="Medium", command=lambda: self.nova_igra(35))
        meni.add_command(label="Hard", command=lambda: self.nova_igra(27))

        menu.add_cascade(label="Pomoč", menu=meni1)
        meni1.add_command(label="Preveri pravilnost vnosov", command=lambda: self.pomoc())
        meni1.add_command(label="Rešitev sudokuja", command=lambda: self.racunalnik_resi())

        menu.add_cascade(label="Razveljavi", menu=meni4)
        meni4.add_command(label="Razveljavi zadnjo potezo", command=lambda: self.razveljavi())
        meni4.add_command(label="Razveljavi igro", command=lambda: self.razveljavi_igro())

        menu.add_cascade(label="Izhod", menu=meni3)
        meni3.add_command(label="Končaj igro", command=master.destroy)


        self.velikost_polja = 50
        self.canvas = Canvas(master, width=9*self.velikost_polja+5, height=9*self.velikost_polja+5)
        self.canvas.grid(row=2, column=0, columnspan=2)
        self.canvas.bind('<Button-1>', self.vpisi)
        master.bind('<Key>', self.nastavi) #ob pritisku na tipkovnico se ob dodatnem pogoju izpiše številka na ustrezno mesto
        master.bind('<BackSpace>', self.razveljavi) #ob pritisku na "backspace" se razveljavi zadnja vnešena številka

        self.nova_igra(50) #nivo 'easy'

    def nastavi(self, event):
        """ob pritisku uporabnika na tipkovnico se številka (med 1 in 9) shrani v spremenjivko vnesena_stevilka"""
        if event.char.isdigit():
            if event.char != '0':
                self.vnesena_stevilka = int(event.char)

    def izpisi_stevilko(self, i, j, stevilka):
        """funkcija, ki na (i, j)-to mesto vpiše številko"""
        if self.sudoku.zacetni_sudoku[i][j] == None:
            self.canvas.create_text(j*self.velikost_polja+1.5+(self.velikost_polja/2), i*self.velikost_polja+1.5+(self.velikost_polja/2), text=str(stevilka), font="Abel 26")
        else:
            self.canvas.create_text(j*self.velikost_polja+1.5+(self.velikost_polja/2), i*self.velikost_polja+1.5+(self.velikost_polja/2), text=str(stevilka), font="Arial 26 bold", fill="#000066")

    def narisi_navodila(self):
        """izpiše navodila na canvas"""
        self.napis.set(" ")
        self.canvas.delete(ALL)
        for i in range(len(self.navodila)):
            self.canvas.create_text(225, i*20 + 40, text=self.navodila[i])

    def narisi_sudoku(self, pomoc=False):
        """funkcija, ki na canvas nariše sudoku polje, v primeru pomoči prej izriše še zelen ali rdeč kvadratek"""
        self.canvas.delete(ALL)
        if not self.se_igra:
            return
        for x in range(0,10):
            if x%3==0:
                self.canvas.create_line(3+x*self.velikost_polja, 0, 3+x*self.velikost_polja, 9*self.velikost_polja+3, width=3)
            else:
                self.canvas.create_line(3+x*self.velikost_polja, 0, 3+x*self.velikost_polja, 9*self.velikost_polja+3, width=1)
            if x%3==0:
                self.canvas.create_line(0, 3+x*self.velikost_polja, 9*self.velikost_polja+5, 3+x*self.velikost_polja, width=3)
            else:
                self.canvas.create_line(0, 3+x*self.velikost_polja, 9*self.velikost_polja+5, 3+x*self.velikost_polja, width=1)

        for i in range(0,9):
            for j in range(0,9):
                if pomoc and self.sudoku.sudoku[i][j] and self.sudoku.zacetni_sudoku[i][j] == None:
                    if self.sudoku.sudoku[i][j] == self.sudoku.resitve[0][i][j]:
                        self.canvas.create_rectangle(self.velikost_polja*j+3, self.velikost_polja*i+3, self.velikost_polja*(j+1)+3, self.velikost_polja*(i+1)+3, fill="green")
                    else:
                        self.canvas.create_rectangle(self.velikost_polja*j+3, self.velikost_polja*i+3, self.velikost_polja*(j+1)+3, self.velikost_polja*(i+1)+3, fill="red")

                if self.sudoku.sudoku[i][j]:
                    self.izpisi_stevilko(i, j, self.sudoku.sudoku[i][j])

    def nova_igra(self, tezavnost):
        """funkcija, ki začne novo igro glede na težavnost"""
        self.se_igra = True
        self.napis.set("")
        self.tezavnost = tezavnost
        nov_sudoku = Sudoku(naredi_sudoku(tezavnost))
        self.sudoku = nov_sudoku
        self.sudoku.vse_resitve()
        self.narisi_sudoku()

    def pravilno_izpoljnjen(self):
        """funkcija, ki v primeru izpoljnjenega sudokuja ugotovi pravilnost rešitve"""
        if self.sudoku.resitve == None:
            self.sudoku.vse_resitve()
        if self.sudoku.sudoku == self.sudoku.resitve[0]:
            return True
        return False

    def konec_igre(self):
        """funkcija, ki se izvede, ko je konec igre"""
        if self.pravilno_izpoljnjen():
            self.napis.set("Čestitke! Pravilno ste izpolnili sudoku.")
        elif self.sudoku.sudoku_resen():
            self.napis.set("Sudoku ni pravilno izpolnjen. Pomagajte si z zavihkom 'Pomoč'.")

    def vpisi(self, event):
        """vpiše izbrano številko na mesto klika"""
        if self.se_igra:
            i = (event.y-3)//self.velikost_polja
            j = (event.x-3)//self.velikost_polja
            if self.sudoku.zacetni_sudoku[i][j] == None and self.sudoku.sudoku[i][j] == None:
                if self.vnesena_stevilka == None:
                    return
                self.sudoku.dodaj_stevilko(self.vnesena_stevilka, i, j)
                self.narisi_sudoku()
                self.zadnja_vnesena = (i, j)
                if self.sudoku.sudoku_resen():
                    self.konec_igre()
            elif self.sudoku.zacetni_sudoku[i][j] == None: #če želi uporabnik zbrisati številko in napisati novo na mesto napake
                self.pobrisi_polje(i,j)
        else:
            return

    def pobrisi_polje(self, i, j):
        """pobriše vrednost v polju, ki jo je dodal uporabnik"""
        if self.sudoku.zacetni_sudoku[i][j] == None:
            self.sudoku.odstrani_stevilko(i,j)
            self.narisi_sudoku()

    def pomoc(self):
        """preveri pravilnost izpolnjenih mest, v primeru, ko napačno izpolnjenih ni preveč"""
        if self.se_igra:
            if self.sudoku.resitve == None:
                self.sudoku.vse_resitve()
            self.narisi_sudoku(True)
        else:
            return

    def razveljavi(self, event=None):
        """razveljavi zadnjo vnešeno številko"""
        if self.se_igra:
            if self.zadnja_vnesena:
                i, j = self.zadnja_vnesena
                self.sudoku.odstrani_stevilko(i, j)
                self.narisi_sudoku()
        else:
            return

    def razveljavi_igro(self):
        """razveljavi vse vnose, ki jih je naredil uporabnik"""
        if self.se_igra:
            zacetni = [[x for x in y] for y in self.sudoku.zacetni_sudoku]
            self.sudoku = Sudoku(zacetni)
            self.narisi_sudoku()
        else:
            return

    def racunalnik_resi(self):
        """prikaže rešitev sudokuja, kot jo vrne računalnik"""
        if self.se_igra:
            if self.sudoku.resitve == None:
                self.sudoku.vse_resitve()
            resitev = Sudoku(self.sudoku.resitve[0], self.sudoku.zacetni_sudoku)
            self.sudoku = resitev
            self.narisi_sudoku()
        else:
            return

master = Tk()
aplikacija = GraficniSudoku(master)
master.mainloop()
