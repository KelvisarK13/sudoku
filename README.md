# SUDOKU #

### Opis igre ###

Sudoku je logična uganka, katere cilj je zapolniti kvadratno mrežo velikosti 9 × 9 s števili od 1 do 9. Vsako število se lahko pojavi točno enkrat v vsakem stolpcu, vsaki vrstici in vsakem manjšem kvadratu velikosti 3 × 3. Za enolično rešitev poskrbijo (na začetku) že zapolnjena mesta.

### Vsebina repozitorija ###

* `razred_sudoku.py`: vsebuje logiko programa (generiranje, reševanje sudokujev)

* `graficni_vmesnik_sudoku.py`: vsebuje grafični vmesnik, ki skrbi za prikaz podatkov in interakcijo z uporabnikom

### O repozitoriju ###

Repozitorij 'Sudoku' vsebuje projekt pri predmetu Programiranje 2 (študijsko leto 2014/2015). Napisan je v programskem jeziku Python 3.4 z uporabo modula Tkinter.